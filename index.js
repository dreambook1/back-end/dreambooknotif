const middleware = require('./sender');
const getUserData = require('./getUserData')
// let data = ["user_id","template", "notifTYPE", "data+"]
// const moq = ["", "test","device",]

// const data = {user_id: "Users/128777", template: "test", notifType: "device"}

// getUserData(data.user_id).then(userData => {
//     const finalData = {...data, userData};
//     middleware(finalData);
// })
var amqp = require('amqplib/callback_api');
require('dotenv').config()

amqp.connect(process.env.RABBIT_MQ, function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = 'dreambook';

    channel.assertQueue(queue, {
      durable: false
    });

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
    channel.consume(queue, function(msg) {
      console.log(" [x] Received %s", msg.content.toString());
      data = JSON.parse(msg.content.toString())
      getUserData(data.user_id).then(userData => {
        const finalData = {...data, userData};
        middleware(finalData);
      })
    }, {
        noAck: true
      });
  });
});
