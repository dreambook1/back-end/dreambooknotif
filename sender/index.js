const TemplateFinder = require('./device/templateManager')

function middleware(data){
    if(data.notifType == "device"){
        TemplateFinder(data);
    }
}

module.exports = middleware