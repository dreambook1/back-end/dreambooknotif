const sendFirebaseNotification = require('./sender');
const test = require('./Template/test');
const test2 = require('./Template/test2');

let allTemplate = {...test, ...test2}

function TemplateFinder(data){
    sendFirebaseNotification(data.userData.fcm_id, allTemplate[data.template].title, allTemplate[data.template].body)
}

module.exports = TemplateFinder;
