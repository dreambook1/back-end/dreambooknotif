const admin = require('firebase-admin');
const serviceAccount = require('../../config/firebase.json');

// Initialize Firebase Admin SDK with your configuration information
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

/**
 * Function to send a Firebase notification.
 * @param registrationToken The device registration token to send the notification to.
 * @param title The title of the notification.
 * @param body The body of the notification.
 * @param data (Optional) Additional data to include with the notification.
 */
async function sendFirebaseNotification(
    registrationToken,
    title,
    body,
    data
) {
    try {
        const message = {
            notification: {
                title: title,
                body: body,
            },
            token: registrationToken,
            data: data || {},
            android: {
                priority: 'high',
                notification: {
                    body: body,
                    title: title,
                    sound: 'default',
                },
            },
            apns: {
                payload: {
                    aps: {
                        alert: {
                            title: title,
                            body: body,
                        },
                        sound: 'default',
                    },
                },
            },
        };

        const response = await admin.messaging().send(message);
        console.log("Msg send")
        return response;
    } catch (error) {
        console.error('Error sending the notification:', error);
        throw error;
    }
}
module.exports = sendFirebaseNotification;
