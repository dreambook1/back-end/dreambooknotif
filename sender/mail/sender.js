function sendEmailViaMailjet(senderEmail, recipientEmail, Template) {
    const Mailjet = require('node-mailjet');
    const mailjet = Mailjet.apiConnect(
        process.env.MAILJET_API_KEY,
        process.env.MAILJET_SECRET_KEY,
    );

    const request = mailjet
            .post('send', { version: 'v3.1' })
            .request({
            Messages: [
                {
                From: {
                    Email: senderEmail,
                    Name: "Devaux Léo"
                },
                To: [
                    {
                    Email: recipientEmail,
                    Name: "passenger 1"
                    }
                ],
                Subject: "Your email flight plan!",
                TextPart: "welcomeTemplate.TextPart",
                HTMLPart: "welcomeTemplate.HTMLPart"
                }
            ]
            })

    request
        .then((result) => {
            console.log(result.body)
        })
        .catch((err) => {
            //console.log(err)
            console.log(err.statusCode)
        })
}


module.exports = sendEmailViaMailjet;
