const TextPart = `
Bienvenue chez dreambook !
`;

const HTMLPart = `
<html>
<head>
    <style>
        body { font-family: Arial, sans-serif; background-color: #f4f4f4; color: #333; }
        .container { width: 80%; margin: auto; background: #fff; padding: 20px; }
        .header { background: #8bc34a; color: white; padding: 10px; text-align: center; }
        .footer { background: #333; color: white; padding: 10px; text-align: center; }
        .content { padding: 20px; text-align: left; }
        a { color: #8bc34a; }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>dreambook</h1>
        </div>
        <div class="content">
            <h2>Bienvenue chez dreambook !</h2>
            <p>Cher [Nom],</p>
            <p>Merci de votre intérêt à rejoindre notre communauté de producteurs locaux et de consommateurs responsables. Ensemble, nous faisons la différence !</p>
            <!-- Ajoutez plus de contenu ici -->
            <p>Cordialement,<br>L'équipe dreambook</p>
        </div>
        <div class="footer">
            <p>Suivez-nous sur <a href="votre-lien-de-réseaux-sociaux">les réseaux sociaux</a></p>
        </div>
    </div>
</body>
</html>`;

module.exports = {
    TextPart,
    HTMLPart
}
