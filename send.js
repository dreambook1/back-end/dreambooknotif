var amqp = require('amqplib/callback_api');

amqp.connect('amqp://guest:guest@167.86.125.173', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = 'dreambook';
    var msg = {user_id: "Users/126186", template: "test", notifType: "device"};

    channel.assertQueue(queue, {
      durable: false
    });

    channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
    console.log(" [x] Sent %s", msg);
  });
});