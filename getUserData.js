const { Database, aql } = require('arangojs');

async function getUserData(id) {
  const db = new Database({
    url: process.env.ARANGODB_URL, 
    databaseName: process.env.ARANGODB_DB, 
    auth: { username: process.env.ARANGODB_USERNAME, password: process.env.ARANGODB_PASSWORD }, 
  });

  try {
    const query1 = aql`FOR user IN Users FILTER user._id == ${id} RETURN user`;
    const cursor1 = await db.query(query1);
    const result1 = await cursor1.next();
    return result1
  } catch (error) {
    console.error('Error executing queries:', error);
  }
}

module.exports = getUserData;
